import React from "react";
import { Switch, Link, Route } from "react-router-dom";
import Calendar from "../Calendar/Calendar";
import Editor from "../Editor/Editor";
import Log from "../Log/Log";
import Main from "../Main/Main";
import Template from "../Template/Template";
import ConsiderUser from "../๊๊UserPermission/ConsiderUser";
import UserGroup from "../๊๊UserPermission/UserGroup";
import "./SideBar.scss";
const SideBar = () => {
  return (
    <div>
      <div className="sidebar">
        <div className="sidebar-header">
          <h1>GSM QUALITY</h1>

          <span className="fa fa-bars"></span>
        </div>
        <div className="sidebar-menu">
          <div className="">
            <ul>
              <li>
                <Link to={"/"}>
                  <span className="fas fa-home"></span>
                  <span>Home</span>
                </Link>
              </li>
            </ul>
            <div className="main-side"></div>
          </div>

          <div className="detail-side">
            <p id="menu-p">INTERFACE</p>
            <ul>
              <li>
                <Link to={"/login_log"}>
                  <span className="fas fa-wrench"></span>
                  <span>Log Login</span>
                </Link>
              </li>
            </ul>
            <div className="main-side"></div>
          </div>
          <div className="foot-side">
            <p id="menu-p">MANAGEMENT</p>
            <ul>
              <li>
                <Link to={"/create_calendar"}>
                  <span className="fas fa-cog"></span>
                  <span>สร้างปฏิทิน</span>
                </Link>
              </li>
              <li>
                <Link to={"/create_template"}>
                  <span className="fas fa-cog"></span>
                  <span>สร้างเทมเพลต</span>
                </Link>
              </li>
              <li>
                <Link to={"/create_editor_page"}>
                  <span className="fas fa-cog"></span>
                  <span>รายการเทมเพลต</span>
                </Link>
              </li>
              <li>
                <Link to={"/consider_user"}>
                  <span className="fas fa-cog"></span>
                  <span>ผู้มีสิทธิใช้งานระบบ</span>
                </Link>
              </li>
              <li>
                <Link to={"/user_group"}>
                  <span className="fas fa-cog"></span>
                  <span>พิจารณาผู้มีสิทธิใช้งานระบบ</span>
                </Link>
              </li>
            </ul>
            <div className="main-side"></div>
          </div>
        </div>
      </div>
      <div className="main-content">
        <header>
          <div className="search-wrapper">
            <span className="fas fa-search"></span>
            <input type="search" placeholder="ค้นหา" />
          </div>
          <div className="social-icons">
            <span className="far fa-bell"></span>
            <span className="far fa-comment-alt"></span>
            <span className="fas fa-user-circle"></span>
          </div>
        </header>
        <main>
          <Switch>
            <Route exact path="/" component={Main} />
            <Route exact path="/login_log" component={Log} />
            <Route exact path="/create_template" component={Template} />
            <Route exact path="/create_editor_page" component={Editor} />
            <Route exact path="/consider_user" component={ConsiderUser} />
            <Route exact path="/user_group" component={UserGroup} />
            <Route exact path="/create_calendar" component={Calendar} />
          </Switch>
        </main>
      </div>
    </div>
  );
};

export default SideBar;
