import React from 'react'
import { BrowserRouter as Router,Switch,Route } from "react-router-dom";
import SideBar from './Components/SideBar/SideBar'
const Routers = () => {
    return (
        <div>
            <Router>
                <SideBar/>
            </Router>
        </div>
    )
}

export default Routers
