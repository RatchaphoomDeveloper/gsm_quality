import React, { useEffect } from "react";
import "./Navbar.scss";
const NavLang = () => {
  const langArr = [
    {
      id: 1,
      langLabel: "ไทย",
      langImg: "/images/thai.png",
    },
    {
      id: 2,
      langLabel: "อังกฤษ",
      langImg: "/images/eng.png",
    },
  ];

  const fontSizeArr = [
    {
      label: "A",
      size: "16px",
    },
    {
      label: "A",
      size: "18px",
    },
    {
      label: "A",
      size: "20px",
    },
  ];
  useEffect(() => {}, []);
  return (
    <div>
   
      <div className="nav-lang-container">
        <div className="nav-lang-flex-01">
          <label className="text-size-label">ขนาดตัวอักษร</label>
          {fontSizeArr.map((res, i) => {
            return (
              <div className="flex-column" key={i}>
                <label className="nav-lang-text" style={{ fontSize: res.size }}>
                  {res.label}
                </label>
              </div>
            );
          })}
        </div>
        <div className="nav-lang-flex-02">
          {fontSizeArr.map((res, i) => {
            return (
              <div className="flex-column hide" key={i}>
                <label className="nav-lang-text">{res.label}</label>
              </div>
            );
          })}
          {langArr.map((res, i) => {
            return (
              <div className="flex-column" key={i}>
                <img
                  src={process.env.PUBLIC_URL + res.langImg}
                  className="flag-container "
                />
                <label className="nav-lang-text">{res.langLabel}</label>
              </div>
            );
          })}
        </div>
      </div>
      
    </div>
  );
};

export default NavLang;
