import React from "react";
import {
  faBars,
  faChartArea,
  faChartLine,
  faHandHoldingUsd,
  faOilCan,
  faRssSquare,
} from "@fortawesome/free-solid-svg-icons";
import { Bar, Pie, Line } from "react-chartjs-2";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./home.scss";
const HomeIndex = () => {
  const [actives, setActives] = React.useState(false);
  const labels = ["JAN", "FEB", "MAR"];
  const data = {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
    datasets: [
      {
        label: "First dataset",
        data: [33, 53, 85, 41, 44, 65],
        fill: true,
        backgroundColor: "rgba(75,192,192,0.2)",
        borderColor: "rgba(75,192,192,1)"
      },
      {
        label: "Second dataset",
        data: [33, 25, 35, 51, 54, 76],
        fill: false,
        borderColor: "#742774"
      }
    ]
  };
  const busArr = [
    {
      label: "การจัดหาและจัดจำหน่ายก๊าซธรรมชาติ",
    },
    {
      label: "โรงแยกก๊าซธรรมชาติ",
    },
    {
      label: "ก๊าซธรรมชาติสำหรับยานยนต์ PTT NGV",
    },
    {
      label: "ระบบท่อส่งก๊าซธรรมชาติ",
    },
  ];

  const newsMenuArr = [
    {
      label: "CSC News",
    },
    {
      label: "ข่าวธุรกิจ",
    },
    {
      label: "ข่าวสังคมสิ่งแวดล้อม",
    },
    {
      label: "ความเคลื่อนไหว",
    },
    {
      label: "สถานการณ์พลังงาน",
    },
  ];

  const newsCardArr = [
    {
      header: "",
      body:
        "กลุ่ม ปตท. เร่งระดมความช่วยเหลือ COVID-19 ระลอก 3 งบประมาณกว่า 30 ล้านบาท",
    },
    {
      header: "",
      body:
        "นายอรรถพล ฤกษ์พิบูลย์ ประธานเจ้าหน้าที่บริหารและกรรมการผู้จัดการใหญ่ บริษัท ปตท. จำกัด (มหาชน) (ปตท.) ",
    },
    {
      header: "",
      body:
        "กลุ่ม ปตท. เร่งระดมความช่วยเหลือ COVID-19 ระลอก 3 งบประมาณกว่า 30 ล้านบาท",
    },
    {
      header: "",
      body:
        "นายอรรถพล ฤกษ์พิบูลย์ ประธานเจ้าหน้าที่บริหารและกรรมการผู้จัดการใหญ่ บริษัท ปตท. จำกัด (มหาชน) (ปตท.) เปิดเผยถึงกรณีผู้บริหาร ปตท. ระดับรองฯ 1 ราย ติดเชื้อไวรัส COVID-19 ส่งผลให้ตนเองและผู้บริหารที่ทำงานใกล้ชิดเข้าข่ายกักตัวเฝ้าระวัง (Tier2) ดังนั้น เพื่อเป็นการแสดงความรับผิดชอบต่อสังคม จึงได้เข้าตรวจหาเชื้อ COVID-19 จากคณะแพทย์ศาสตร์ วชิรพยาบาล ในช่วงเย็นของวันที่ 9 เมษายน 2564 และในวันนี้​ได้รับแจ้งผลการตรวจหาเชื้อ COVID-19 แล้วพบว่า ผลเป็นลบ ไม่พบเชื้อ COVID-19 แต่ทั้งนี้ ยังคงต้องกักตัวให้ครบ 14 วัน ตามมาตรการ",
    },
    {
      header: "",
      body:
        "กลุ่ม ปตท. เร่งระดมความช่วยเหลือ COVID-19 ระลอก 3 งบประมาณกว่า 30 ล้านบาท",
    },
    {
      header: "",
      body:
        "นายอรรถพล ฤกษ์พิบูลย์ ประธานเจ้าหน้าที่บริหารและกรรมการผู้จัดการใหญ่ บริษัท ปตท. จำกัด (มหาชน) (ปตท.) เปิดเผยถึงกรณีผู้บริหาร ปตท. ระดับรองฯ 1 ราย ติดเชื้อไวรัส COVID-19 ส่งผลให้ตนเองและผู้บริหารที่ทำงานใกล้ชิดเข้าข่ายกักตัวเฝ้าระวัง (Tier2) ดังนั้น เพื่อเป็นการแสดงความรับผิดชอบต่อสังคม จึงได้เข้าตรวจหาเชื้อ COVID-19 จากคณะแพทย์ศาสตร์ วชิรพยาบาล ในช่วงเย็นของวันที่ 9 เมษายน 2564 และในวันนี้​ได้รับแจ้งผลการตรวจหาเชื้อ COVID-19 แล้วพบว่า ผลเป็นลบ ไม่พบเชื้อ COVID-19 แต่ทั้งนี้ ยังคงต้องกักตัวให้ครบ 14 วัน ตามมาตรการ",
    },
  ];
  return (
    <div>
      {window.location.pathname === "/" && (
        <section>
          <div className="graph-bar-container">
            <div className="graph-detail-container">
              <div className="graph-detail-list">
                STOCK{" "}
                <span>
                  <FontAwesomeIcon
                    icon={faChartLine}
                    style={{ fontSize: "20px" }}
                  />
                </span>{" "}
                <div className="stocking-container">38,780 BTH</div>
              </div>
              <div className="graph-detail-list">
                FINANCE{" "}
                <span>
                  <FontAwesomeIcon icon={faChartArea} />
                </span>{" "}
                <div className="finace-container">0.032 USD FOR 1 THB</div>
              </div>
              <div className="graph-detail-list">
                FUEL PRICE{" "}
                <span>
                  <FontAwesomeIcon icon={faOilCan} />
                </span>{" "}
                <div className="fuel-container">GAS. 91 26.38/L</div>
              </div>
              <div className="graph-detail-list">
                GOLD PRICE{" "}
                <span>
                  <FontAwesomeIcon icon={faHandHoldingUsd} />
                </span>{" "}
                <div className="gold-container">GOLD 96.5%</div>
              </div>
              <div className="graph-detail-list">
                NEWS{" "}
                <span>
                  <FontAwesomeIcon icon={faRssSquare} />
                </span>{" "}
                <div className="news-container">READ MORES</div>
              </div>
            </div>
          </div>
        </section>
      )}

      <div className="graph-containers">
        <div className="home2-container">
          <div>
            <h1 className="h1">ธุรกิจของเรา</h1>
            <ul className="ui-item-cpontainer2">
              {busArr.map((res, i) => {
                return (
                  <div>
                    <li key={i} className="li-item">
                      {res.label}
                    </li>
                    <div className="underline"></div>
                    <div style={{ clear: "both" }}></div>
                  </div>
                );
              })}
            </ul>
          </div>
          <div>
            {" "}
            <div className="home-animation-container">
              <div className="radius-container-1">
                <label className="graph-header mt-50-1">
                  ยอดของการผลิต / วัน
                </label>
                <div className="detail-in-radius">
                  <Bar
                    data={{
                      labels: [
                        "Red",
                        "Blue",
                        "Yellow",
                        "Green",
                        "Purple",
                        "Orange",
                      ],
                      datasets: [
                        {
                          label: "# of Votes",
                          data: [12, 19, 3, 5, 2, 3],
                          backgroundColor: [
                            "rgba(255, 99, 132, 0.2)",
                            "rgba(54, 162, 235, 0.2)",
                            "rgba(255, 206, 86, 0.2)",
                            "rgba(75, 192, 192, 0.2)",
                            "rgba(153, 102, 255, 0.2)",
                            "rgba(255, 159, 64, 0.2)",
                          ],
                          borderColor: [
                            "rgba(255, 99, 132, 1)",
                            "rgba(54, 162, 235, 1)",
                            "rgba(255, 206, 86, 1)",
                            "rgba(75, 192, 192, 1)",
                            "rgba(153, 102, 255, 1)",
                            "rgba(255, 159, 64, 1)",
                          ],
                          borderWidth: 1,

                         
                        },
                      ],
                    }}
                    height={400}
                    width={600}
                    options={{
                      responsive: true,
                      scales: {
                        xAxes: [
                          {
                            ticks: { display: true },
                            gridLines: {
                              display: false,
                              drawBorder: false
                            }
                          }
                        ],
                        yAxes: [
                          {
                            ticks: { display: false },
                            gridLines: {
                              display: false,
                              drawBorder: false
                            }
                          }
                        ]
                      }
                    }}
                  />
                </div>
              </div>
              <div className="radius-container-2">
                <div className="detail-in-radius">
                  <Pie
                    data={{
                      labels: ["Red", "Blue", "Yellow"],
                      datasets: [
                        {
                          label: "My First Dataset",
                          data: [300, 50, 100],
                          backgroundColor: [
                            "rgb(255, 99, 132)",
                            "rgb(54, 162, 235)",
                            "rgb(255, 205, 86)",
                          ],
                          hoverOffset: 4,
                        },
                      ],
                    }}
                    height={100}
                    width={100}
                    options={{
                      options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                          xAxes: [
                            {
                              ticks: { display: true },
                              gridLines: {
                                display: false,
                                drawBorder: false,
                              },
                            },
                          ],
                          yAxes: [
                            {
                              ticks: { display: false },
                              gridLines: {
                                display: false,
                                drawBorder: false,
                              },
                            },
                          ],
                        },
                      },
                    }}
                  />
                </div>
                <label className="graph-header mt-50 ">
                  การผลิตคิดเป็นร้อยละ
                </label>
              </div>
              <div className="radius-container-3">
                <label className="graph-header">การเจริญเติบโตของธุรกิจ</label>
                <div className="detail-in-radius">
                  <Line
                   data={data}
                    height={100}
                    width={100}
                    options={{
                      scales: {
                        xAxes: [
                          {
                            ticks: { display: true },
                            gridLines: {
                              display: false,
                              drawBorder: false
                            }
                          }
                        ],
                        yAxes: [
                          {
                            ticks: { display: false },
                            gridLines: {
                              display: false,
                              drawBorder: false
                            }
                          }
                        ]
                      }
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <div className="home-container">
          <div className="home-list-buss-item">
            <h1 className="h1">ธุรกิจของเรา</h1>
            <ul className="ui-item-container">
              {busArr.map((res, i) => {
                return (
                  <div>
                    <li key={i} className="li-item-list">
                      {res.label}
                    </li>
                    <div style={{clear:"both"}}></div>
                  </div>
                );
              })}
            </ul>
          </div>
          <div className="home-sea-container">
            <div className="home-animation-container" >
              <div className="radius-container-1">
              </div>
              <div className="radius-container-2">

              </div>
              <div className="radius-container-3">
                
              </div>
            </div>
            <video autoPlay loop={true} muted={true} id={"video-1"}>
              <source
                src={process.env.PUBLIC_URL + "/images/sea.mp4"}
                type="video/mp4"
              />
            </video>
          </div>
        </div> */}
        <section>
          <div className="news-containers">
            <div className="news-flex-01">
              <h1 className="news-h1">ข่าวของ PTT GSM</h1>
              <ui className="news-ui-item">
                {newsMenuArr.map((res, i) => {
                  return (
                    <li
                      key={i}
                      className="news-li-list"
                      onClick={() => {
                        setActives(!actives);
                      }}
                    >
                      {res.label}
                    </li>
                  );
                })}
              </ui>
            </div>
            <div className="news-flex-02">
              <div className="news-card-container">
                {newsCardArr.map((res, i) => {
                  return (
                    <div key={i}>
                      {actives ? (
                        <div className="news-card-item fade-out">
                          <img
                            src={process.env.PUBLIC_URL + "/images/gas.jpeg"}
                            className="card-img fade-out-img"
                          />
                          <label className="news-card-label">{res.body}</label>
                        </div>
                      ) : (
                        <div className="news-card-item fade-out-img">
                          <img
                            src={process.env.PUBLIC_URL + "/images/gas.jpeg"}
                            className="card-img fade-in-img"
                          />
                          <label className="news-card-label">{res.body}</label>
                        </div>
                      )}
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </section>
        <section>
          <div className="read-more-container"> ดูเพื่มเติม</div>
        </section>
      </div>
    </div>
  );
};

export default HomeIndex;
