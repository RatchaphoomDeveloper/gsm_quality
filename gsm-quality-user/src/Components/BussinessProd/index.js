import React from "react";
import { Doughnut } from 'react-chartjs-2'
import "./bussinessprod.scss";
const BussinessProdIndex = () => {
  const [tab1, setTab1] = React.useState(true);
  const [tab2, setTab2] = React.useState(false);
  const [tab3, setTab3] = React.useState(false);
  const data = {
    labels: [
      'Red',
      'Blue',
      'Yellow'
    ],
    datasets: [{
      label: 'My First Dataset',
      data: [300, 50, 100],
      backgroundColor: [
        'rgb(255, 99, 132)',
        'rgb(54, 162, 235)',
        'rgb(255, 205, 86)'
      ],
      hoverOffset: 4
    }]
  };
  return (
    <div>
      <div className="buss-rout-container-name ">
        <div className="border-under-head ">
          <h1>นักลงทุนสัมพันธ์</h1>
        </div>
      </div>

      <div className="top-buss-container">
        <div>
          <h2> ข้อมูลสำคัญทางการเงิน</h2>
        </div>
      </div>
      <div className="bussiness-container">
        <div className="buss-item1">
          <div className="buss-item-1-header">
            <label className="buss-item-label-1">ผลการดำเนินงานของธุรกิจ</label>
            <label className="buss-item-label-2">
              ( ณ วันที่ 31 ธ.ค. 2563 )
            </label>
          </div>
          <div className="buss-item-body-container">
            <div className="diff-container">
              <div className="img-circle"></div>
              <div className="img-bottom">PTT Stattions</div>
            </div>

            <div className="lines"></div>
            <div className="diff-container">
              <div className="img-circle"></div>
              <div className="img-bottom">Natural Gas</div>
            </div>
            <div className="lines"></div>
            <div className="diff-container">
              <div className="img-circle"></div>
              <div className="img-bottom">Oil Rig</div>
            </div>
          </div>
          <div className="footer-buss-container">
            <label className="footer-buss-text">
              หมายเหตุ: จำนวนรวมในประเทศและต่างประเทศ
            </label>
          </div>
        </div>
        <div className="buss-item2">
          <div className="tab-header">
            <div
              className={tab1 === true ? "active" : ""}
              onClick={() => {
                setTab1(true);
                setTab2(false);
                setTab3(false);
              }}
            >
              <span>รายได้</span>
            </div>
            <div
              className={tab2 === true ? "active" : ""}
              onClick={() => {
                setTab1(false);
                setTab2(true);
                setTab3(false);
              }}
            >
              <span>EBITDA</span>
            </div>
            <div
              className={tab3 === true ? "active" : ""}
              onClick={() => {
                setTab1(false);
                setTab2(false);
                setTab3(true);
              }}
            >
              <span>กำไรสุทธิ</span>
            </div>
          </div>
          {/* <div className="tab-indecator">
            
          </div> */}
          <div className="tab-body">
            <div className={tab1 === true ? "active" : ""}>
              <div className="tab1-container">
                <div className="tab1-header1">
                  <h5 className="year-growth">2563</h5>
                </div>
                <div className="tab1-header2">
                  <label className="label-number">หน่วย: ล้านบาท</label>
                </div>
              </div>
              <div className="tab1-body-container">
                <div className="tab1-table-container">
                  <table>
                    <thead></thead>
                    <tbody>
                      <tr id="tr-style">
                        <td id="item-1">ธุรกิจน้ำมัน</td>
                        <td id="item-2">396,708.00</td>
                      </tr>
                      <tr id="tr-style">
                        <td id="item-1">ธุรกิจค้าปลีกสินค้าและบริการอื่นๆ</td>
                        <td id="item-2">16,867.00</td>
                      </tr>
                      <tr id="tr-style">
                        <td id="item-1">ธุรกิจต่างประเทศ</td>
                        <td id="item-2">21,361.00</td>
                      </tr>
                      <tr id="tr-style">
                        <td id="item-1">ธุรกิจอื่น ๆ</td>
                        <td id="item-2">1,609</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className="tab1-graph-container">
                  <Doughnut data={data} options={{legend:false}} width={200} height={200} />
                </div>
              </div>
            </div>
            <div className={tab2 === true ? "active" : ""}>
              <div className="tab1-container">
                <div className="tab-item1"></div>
                <div className="tab-item2"></div>
              </div>
            </div>
            <div className={tab3 === true ? "active" : ""}>
            <div className="tab1-container">
                <div className="tab1-header1">
                  <h5 className="year-growth">2563</h5>
                </div>
                <div className="tab1-header2">
                  <label className="label-number">หน่วย: ล้านบาท</label>
                </div>
              </div>
              <div className="tab1-body-container">
                <div className="tab1-table-container">
                  <table>
                    <thead></thead>
                    <tbody>
                      <tr id="tr-style">
                        <td id="item-1">ธุรกิจน้ำมัน</td>
                        <td id="item-2">396,708.00</td>
                      </tr>
                      <tr id="tr-style">
                        <td id="item-1">ธุรกิจค้าปลีกสินค้าและบริการอื่นๆ</td>
                        <td id="item-2">16,867.00</td>
                      </tr>
                      <tr id="tr-style">
                        <td id="item-1">ธุรกิจต่างประเทศ</td>
                        <td id="item-2">21,361.00</td>
                      </tr>
                      <tr id="tr-style">
                        <td id="item-1">ธุรกิจอื่น ๆ</td>
                        <td id="item-2">1,609</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className="tab1-graph-container">
                  <Doughnut data={data} options={{legend:false}} width={200} height={200} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="top-buss-container">
        {/* <div>
          <h2> เอกสารดาวโหลดสำคัญ</h2>
        </div> */}
      </div>
    </div>
  );
};

export default BussinessProdIndex;
