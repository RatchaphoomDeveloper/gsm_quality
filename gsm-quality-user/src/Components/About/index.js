import React from "react";
import "./about.scss";
const AboutIndex = () => {
  return (
    <div>
      <div className="bg-top">
        <div className="about-container">
          <div className="about-container-flex-01">
            <div className="detail-container">
              <h1 className="about-header-container">วิสัยทัศน์</h1>
              <div className="border-bottom-width"></div>
              <label className="about-text-container">
                แบรนด์ไทยชั้นนำระดับโลกที่สร้างคุณค่าให้กับชุมชนผ่านการดำเนินธุรกิจน้ำมัน
                ธุรกิจค้าปลีก และธุรกิจที่เกี่ยวเนื่อง
              </label>
            </div>
            <div className="detail-container">
              <h1 className="about-header-container">พันธกิจ</h1>
              <div className="border-bottom-width"></div>
              <label className="about-text-container">
                ดำเนินธุรกิจน้ำมัน ธุรกิจค้าปลีก
                และธุรกิจที่เกี่ยวเนื่องทั้งในและต่างประเทศ
                ในฐานะบริษัทชั้นนำระดับสากลที่สร้างคุณค่าและการมีส่วนร่วมให้กับผู้มีส่วนได้ส่วนเสียทุกกลุ่ม
              </label>
            </div>
          </div>
          <div className=" about-container-flex-02">
            <div className="circle-container"></div>
            <div className="circle-container-under"></div>
          </div>
        </div>
        <div>
          <div className="section2-container">
            <div className="space-1">
              <div className="border-detail">
                <div className="header-details" >
                  <label className="green" >ต่อประเทศ</label>
                </div>
                <div>
                  <label className="body-detail" >
                    สร้างการเจริญเติบโตทางเศรษฐกิจอย่างยั่งยืน
                    ผ่านการดำเนินธุรกิจน้ำมัน ธุรกิจค้าปลีก
                    และธุรกิจเกี่ยวเนื่อง
                  </label>
                </div>
              </div>
              <div className="border-detail">
                <div className="header-details">
                  <label className="blue">ต่อผู้ถือหุ้น</label>
                </div>
                <div>
                  <label className="body-detail" >
                    ดำเนินธุรกิจเชิงพาณิชย์ สามารถสร้างผลตอบแทนที่ดี
                    และมีการขยายธุรกิจสู่สากลให้เติบโตต่อเนื่องอย่างยั่งยืน
                  </label>
                </div>
              </div>
              <div className="border-detail">
                <div className="header-details">
                  <label className="hard-blue">ต่อคู่ค้า</label>
                </div>
                <div>
                  <label className="body-detail" >
                    ดำเนินธุรกิจเชิงพาณิชย์ สามารถสร้างผลตอบแทนที่ดี
                    และมีการขยายธุรกิจสู่สากลให้เติบโตต่อเนื่องอย่างยั่งยืน
                  </label>
                </div>
              </div>
            </div>
            <div className="space-2">
              <div className="border-detail">
                <div className="header-details">
                  <label className="red">ต่อสังคมชุมชน</label>
                </div>
                <div>
                  <label className="body-detail" >
                    เป็นองค์กรที่ดีของสังคม
                    ดำเนินธุรกิจที่มีการบริหารจัดการผลกระทบต่อสิ่งแวดล้อมและมีส่วนร่วมในการพัฒนาคุณภาพชีวิตที่ดีแก่สังคม
                    ชุมชน
                  </label>
                </div>
              </div>
              <div className="border-detail">
                <div className="header-details">
                  <label className="orange">ต่อลูกค้า</label>
                </div>
                <div>
                  <label className="body-detail" >
                    สร้างความพึงพอใจและความผูกพันแก่ลูกค้าโดยผ่านการนำเสนอผลิตภัณฑ์และบริการที่มีคุณภาพในระดับมาตรฐานสากล
                  </label>
                </div>
              </div>
              <div className="border-detail">
                <div className="header-details">
                  <label className="yellow">ต่อพนักงาน</label>
                </div>
                <div>
                  <label className="body-detail" >
                    สนับสนุนการพัฒนาความสามารถการทำงานระดับมืออาชีพอย่างต่อเนื่อง
                    ให้ความมั่นใจในคุณภาพชีวิต
                    การทำงานของพนักงานทัดเทียมบริษัทชั้นนำเพื่อสร้างความผูกพันต่อองค์กร
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div>
            
        </div>
      </div>
    </div>
  );
};

export default AboutIndex;
