import React from "react";
import Routers from "./Router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookF,
  faInstagram,
  faLine,
} from "@fortawesome/free-brands-svg-icons";

import "./App.scss";
import { faPrint } from "@fortawesome/free-solid-svg-icons";

function App() {
  return (
    <div className="App">
      <Routers />
   
      <div className="social-bar-container">
        <ui className="ui-container">
          <li className="social-item-1">
            <label className="social-text">FACEBOOK</label>
            <FontAwesomeIcon icon={faFacebookF} />{" "}
          </li>
          <li className="social-item-1">
            <label className="social-text">LINE</label>
            <FontAwesomeIcon icon={faLine} />
          </li>
          <li className="social-item-1">
            <label className="social-text">CONTACT</label>
            <FontAwesomeIcon icon={faPrint} />
          </li>
          <li className="social-item-1">
            <label className="social-text">INSTRAGRAM</label>
            <FontAwesomeIcon icon={faInstagram} />{" "}
          </li>
        </ui>
      </div>
    </div>
  );
}

export default App;

//  <li className="social-item">
//           <FontAwesomeIcon icon={faLine} />
//         </li>
//         <li className="social-item">
//           <FontAwesomeIcon icon={faPrint} />
//         </li>
//         <li className="social-item">
//           <FontAwesomeIcon icon={faInstagram} />
//         </li>
