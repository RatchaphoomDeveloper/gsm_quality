import { faAngleUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState, useEffect } from "react";
import './Scroll.scss'
const Scrolls = ({showBelow}) => {
  const [show, setShow] = useState(showBelow ? false : true);
  const handleScroll = () => {
    if (window.pageXOffset > showBelow) {
      if (!show) setShow(true);
    } else {
      if (show) setShow(false);
    }
  };
  useEffect(()=>{
      if(showBelow){
          window.addEventListener(`scroll`,handleScroll)
          return () => window.removeEventListener(`scroll`, handleScroll);
      }
  })
  const handleClick = () => {
    window[`scrollTo`]({ top: 0, behavior: `smooth` });
  };
  return (
    <div>
      {show && (
        <div id="scroll" onClick={handleClick}>
          <FontAwesomeIcon icon={faAngleUp} />
        </div>
      )}
    </div>
  );
};

export default Scrolls;
