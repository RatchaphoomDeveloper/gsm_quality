import React from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import AboutIndex from './Components/About';
import BussinessProdIndex from './Components/BussinessProd';
import Footerindex from './Components/Footer';
import HomeIndex from './Components/Home';
import Index from "./Components/Navbar/Index";
import NavLang from './Components/Navbar/NavLang';
import SignInIndex from './Components/SignIn';

const Routes =()=>{
    return (
      <Switch>
        <Route exact path={"/"} component={HomeIndex} />
        <Route exact path={"/about"} component={AboutIndex} />
        <Route exact path={"/business"} component={BussinessProdIndex} />
        <Route exact path={"/sign-in"} component={SignInIndex} />
      </Switch>
    );
}

const Routers = () => {
    return (
      <div>
        <Router>
          {/* <NavLang/> */}
          <header>
            <Index />
          </header>

          <Routes />
          <footer>
            <Footerindex/>
          </footer>
        </Router>
      </div>
    );
}

export default Routers
